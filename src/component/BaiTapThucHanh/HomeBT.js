import React, { Component } from 'react'
import FooterTH from './FooterTH'
import HeaderBT from './HeaderBT'
import Midder from './Midder'

export default class HomeBT extends Component {
    render() {
        return (
            <div>
                <HeaderBT/>
                <Midder/>
                <FooterTH/>
            </div>
        )
    }
}
